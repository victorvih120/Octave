# OCTAVE

![Octave](octave.svg "https://www.gnu.org/software/octave/")

The aim of the proposed project is given examples of octave for engineering applications.

Octave is:

+ Powerful mathematics-oriented syntax with built-in 2D/3D plotting and visualization tools
+ [Free software](https://www.gnu.org/software/octave/), runs on GNU/Linux, macOS, BSD, and Microsoft Windows
+ Drop-in compatible with many Matlab scripts

## Exercise  

**1. Pipe Diameter**: The exercise is based on an example from [YouTube](https://youtu.be/cPDH_eQTnS0) related to [the mathematical method of successive approximations](https://www.wikiwand.com/en/Picard%E2%80%93Lindel%C3%B6f_theorem) or [método de aproximaciones sucesivas](https://www.wikiwand.com/es/M%C3%A9todo_de_aproximaciones_sucesivas_de_Picard) in Spanish. 

**2. Bending Moment**: The exercise is based on ideas from [Marcelo Pardo](https://marcelopardo.com/ejemplo-1-analisis-de-solicitaciones-de-momento-flector-y-cortante-en-viga-isostatica/) and the video is shown on [YouTube](https://www.youtube.com/embed/JUMr8XER9h0) and it is related to Octave uses.


