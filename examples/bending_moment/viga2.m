% Parámetros de distancia 
x1 = linspace(0,2,10);
x2 = linspace(2,4,10);
x3 = linspace(4,5.5,10); 
x_total = horzcat(x1,x2,x3);

% Parámetros de momento flector
M1 = 6.5625*x1;
M2 = -15*x2.^2 + 66.5625*x2 - 60;
M3 = -15*x3.^2 + 165*x3 -453.75;
M_total = horzcat(M1,M2,M3);

plot(x_total, M_total, "--0","linewidth",1.5)
xlabel("Longitud [m]", 'FontSize', 8);
ylabel("Momento [kN-m]",'FontSize', 8); 

%plot for axes properties
h=get(gcf, "currentaxes");
set(h, "fontsize", 8, "linewidth", 1, "ydir", "reverse");
set(h, 'xaxislocation', "origin");
set(gcf, 'PaperUnits', 'inches');
set(gcf,'PaperSize', [4 2]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 4 2]);
box off;

print viga.pdf;
