# PIPE DIAMETER EXERCISE

The following exercise is based on a Spanish problem and it is possible to see [online](https://youtu.be/cPDH_eQTnS0).

### Ejercicio de Mecánica de Fluidos

***Aplicación en cálculo de diámetro de tubería***

El cliente solicita un caudal de $`15 - 20~[L/s]`$ para su uso, el agua se va a transportar desde una embalse a una altura de $`z_1`$ de  $`50~[m]`$ por un tubería con una longitud total de $`84~[m]`$ y una rugosidad de $`0.0015~[mm]`$ hasta un lugar con una altura $`z_2`$ de $`1.5~[m]`$. Las características del agua son: 

```math
\rho = 998 [kg/m^3] \\

\mu = 0.001005 [Pa \times s]
```


